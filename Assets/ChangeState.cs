﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeState : MonoBehaviour
{
    [SerializeField]
    private string Newstate="FirstState";

    private void OnMouseDown()
    {
        StartSceneInformation.CameraTag = Newstate;
    }
}
