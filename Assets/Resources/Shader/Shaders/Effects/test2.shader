﻿Shader "Custom/test2"
{
    Properties
    {
	_MainTex("Image (RGB)",2D) = "white"{}
	_EffectIntensity("Effect power",float) = 0.001
    }
    SubShader
    {
        Tags { "RenderType"="Opaque" }
        LOD 200

        CGPROGRAM

        // Use shader model 3.0 target, to get nicer looking lighting
        #pragma target 3.0

		#pragma surface surf Lambert vertex:vert addshadow


		sampler2D _MainTex;
		uniform float _EffectIntensity;
        struct Input
        {
            float2 uv_MainTex;
        };

 
       
        UNITY_INSTANCING_BUFFER_START(Props)
            // put more per-instance properties here
        UNITY_INSTANCING_BUFFER_END(Props)


			void vert(inout appdata_full v) {
			float4 vv = mul(unity_ObjectToWorld, v.vertex);
			vv.xyz -= _WorldSpaceCameraPos.xyz;

			vv = float4(0.0f, (vv.z*vv.z)*-_EffectIntensity, 0.0f, 0.0f);

			v.vertex += mul(unity_WorldToObject, vv);
		}


        void surf (Input IN, inout SurfaceOutput o)
        {
            // Albedo comes from a texture tinted by color
            fixed4 c = tex2D (_MainTex, IN.uv_MainTex);
            o.Albedo = c.rgb;
            o.Alpha = c.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}






/*


Shader"Custom/BlendMods/Screen"{
	Properties{
		_Color("Main Color", Color)=(1,1,1,1)
		_MainTexture("Image (RGB)",2D) = "White"{}
	}
}



Shader"Custom/BlendMode/Dark"{

}




Shader"Custom/BlendMode/Diffuse"{

}




Shader"Custopm/LignhtsMode/PixelLight"{

}




Shader"Custom/LigntMode/NormalLight"{

}



Shader"Custom/LightMode/BleekLight"{

}



Shader"Custom/GeometryMode/Parallax"{
	Properties{
		_MainTexture("Image (RGB)") = "White"{}
	_EffectIntensity("EffectIntensety",float) = 0.001f;
	}
}




Shader"Cistom/Geometry/SpaceCorrupt"{
	Properties{
		_MainTexture("Image (RGB)") = "White"{}
	_EffectIntesety("EffectIntensety",float) = 0.001f;
	}
}





Shader"Custom/Dizzy"{
	Properties{
	_Color("Main Color",Color) = (1,1,1,1)
	_MainTexture("Image (RGB)", 2D) = "white"{}
	_Rings("RingsOfDizzy rings", int) = 0
	_Time("Time TimeDellay",float) = 0.001
	}

	SubShader{
		Tags = {"RenderType"="Opaque" "Queue"="Geometry"}
		LOD 200


		

		CGPROGRAM

		#pragma Surface surf Lambert
		
		Pass{
		
	#pragma vertex vert
	#pragma fragment frag


		struct Input {

			};


		void vert() {

         }

	void frag() {

        }

    	}

		void surf(Input IN,inout SurfaceOutput o) {


		}

		ENDCG

	}
		FallBack "Diffuse"
} 
*/

