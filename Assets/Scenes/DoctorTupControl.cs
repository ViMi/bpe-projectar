﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

public class DoctorTupControl : MonoBehaviour
{
    [SerializeField]
    private float Speed = 5f;

    [SerializeField]
    private float JumpForce = 300f;

    //что бы эта переменная работала добавьте тэг "Ground" на вашу поверхность земли
    private bool IsGrounded;
    private Rigidbody Rigid;


    private Animator anim;

    [SerializeField]
    private GameObject gm;


    void Start()
    {
        Rigid = gm.GetComponent<Rigidbody>();
        anim = gm.GetComponent<Animator>();
    }

    [SerializeField]
    private AnimationClip animation;

    private void Update()
    {

        if (Input.touchCount > 0)
        {
            gm.GetComponent<Animator>().SetFloat("Spead", 1.0f);
            gm.GetComponent<Animator>().SetFloat("Walk", 3f);
            gm.transform.Translate(new Vector3(0, 0, -2) * Speed * Time.deltaTime);
        }


        if (Input.GetMouseButtonDown(0)||I==1)
        {

                gm.GetComponent<Animator>().SetFloat("Spead", 1.0f);
                gm.GetComponent<Animator>().SetFloat("Walk", 3f);

                gm.transform.Translate(new Vector3(0, 0, -2) * Speed * Time.deltaTime);
            
        }
        if(Input.GetMouseButtonUp(0))
        {
            gm.GetComponent<Animator>().SetFloat("Walk", 0f);
        }

    }

    private int I;

    public void Doctor(int i)
    {


        if (i == 5)
        {
            I = 9;
        //    gm.GetComponent<Animator>().SetBool("Atack", false);
          //  gm.GetComponent<Animator>().SetBool("Atack", true);
        }


     //   if (Input.GetKeyUp(KeyCode.W)) anim.SetFloat("Walk", 0f);
        if (i==1)//Move
        {
            I = i;
            // float moveHorizontal = Input.GetAxis("Horizontal");
            // float moveVertical = Input.GetAxis("Vertical");
            // Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
       
        }

        if (i==2)//Turn
        {
            gm.transform.Rotate(0, 90, 0);
        }
        if (i==3)//TurnLeft
        {
            gm.transform.Rotate(0, -90, 0);
        }

    }





    private void MovementLogic()
    {

        if (Input.GetKeyDown(KeyCode.C))
        {
            anim.SetBool("Atack", true);

        }

        else if (Input.GetKeyDown(KeyCode.W))
        {
            // float moveHorizontal = Input.GetAxis("Horizontal");
            // float moveVertical = Input.GetAxis("Vertical");

            // Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);



            transform.Translate(Vector3.back * Speed * Time.deltaTime);
            anim.SetBool("Walk", true);

        }


        else if (Input.GetKeyDown(KeyCode.D))
        {
            transform.Rotate(0, 90, 0);
        }

        else if (Input.GetKeyDown(KeyCode.A))
        {
            transform.Rotate(0, -90, 0);
        }
    }


    private void JumpLogic()
    {
        if (Input.GetAxis("Jump") > 0)
        {
            if (IsGrounded)
            {
                Rigid.AddForce(Vector3.up * JumpForce);
            }
        }
    }


    void OnCollisionEnter(Collision collision)
    {
        IsGroundedUpate(collision, true);
    }

    void OnCollisionExit(Collision collision)
    {
        IsGroundedUpate(collision, false);
    }

    private void IsGroundedUpate(Collision collision, bool value)
    {
        if (collision.gameObject.tag == ("Ground"))
        {
            IsGrounded = value;
        }
    }

}
