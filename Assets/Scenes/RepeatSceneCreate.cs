﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RepeatSceneCreate : MonoBehaviour
{
    [SerializeField]
    private GameObject gm;

    [SerializeField]
    private GameObject OldGm;

    [SerializeField]
    private GameObject Parent;


    private Transform tr;
    public void Repeat()
    {
        tr = OldGm.transform;
        Destroy(OldGm);
        (OldGm = (Instantiate(gm, tr))).transform.parent=Parent.transform;

    }
}
