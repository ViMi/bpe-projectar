﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartSceneInformation : MonoBehaviour
{
    public static string CameraTag="FirstState";

    [SerializeField]
    private GameObject[] gm;

    [SerializeField]
    private Camera[] camera;

    private void Start()
    {
        if (CameraTag == "SecondState")
        {
            camera[0].enabled = false;
            camera[1].enabled = true;

        //    gm[0].SetActive(false);
            gm[1].SetActive(true);
        }
    }
}
