﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Threading;
using MMGWorld;
using System.IO;
using UnityEngine;

public class ReadCard : MonoBehaviour
{

   public static void HeroInit( GameObject gameObject)
    {
        CultureInfo NewCult = (CultureInfo)Thread.CurrentThread.CurrentCulture.Clone();
        NewCult.NumberFormat.NumberDecimalSeparator = ".";
        Thread.CurrentThread.CurrentCulture = NewCult;


        

        TextAsset path = Resources.Load<TextAsset>(
          (gameObject.GetComponent<HeroInformation>().Player == 1 ? ReadCardSystem.DeckOfCarts[gameObject.GetComponent<HeroInformation>().Index] 
            : ReadCardSystem.DeckOfEnemy[gameObject.GetComponent<HeroInformation>().Index]));


        gameObject.GetComponentInChildren<Renderer>().material = Resources.Load("Materials/" + "Card" +
            (gameObject.GetComponent<HeroInformation>().Player == 1 ? ReadCardSystem.DeckOfCarts[gameObject.GetComponent<HeroInformation>().Index] :
            ReadCardSystem.DeckOfEnemy[gameObject.GetComponent<HeroInformation>().Index])) as Material;



    //    string[] FileCharacteristic = File.ReadAllLines(path.text);
       string[] FileCharacteristic = path.text.Split('\n');

        WorldObject.MainThreits mainThreits = new Hero.MainThreits(int.Parse(FileCharacteristic[4]), int.Parse(FileCharacteristic[5]), FileCharacteristic[1], FileCharacteristic[2], FileCharacteristic[3], int.Parse(FileCharacteristic[6]));



        WorldObject.Vulnarability vulnarability = new Armor.Vulnarability(float.Parse(FileCharacteristic[22]), float.Parse(FileCharacteristic[19]), float.Parse(FileCharacteristic[20]), float.Parse(FileCharacteristic[21]));


        Weapon.Damage damage = new Weapon.Damage(int.Parse(FileCharacteristic[12]), int.Parse(FileCharacteristic[10]), int.Parse(FileCharacteristic[9]), int.Parse(FileCharacteristic[11]));
        Weapon.WeaponDamage weaponDamage = new Weapon.WeaponDamage(damage, damage);
        Weapon.WeaponConsuming weaponConsuming = new Weapon.WeaponConsuming(0, 10);
        Weapon weapon = new Weapon(weaponDamage, weaponConsuming);


        Armor.Defense defense = new Armor.Defense(float.Parse(FileCharacteristic[17]), float.Parse(FileCharacteristic[15]), float.Parse(FileCharacteristic[14]), float.Parse(FileCharacteristic[16]));
        Armor armor = new Armor(defense);



        //Nulable type
        gameObject.GetComponent<HeroInformation>().hero = new Hero(weapon, armor, mainThreits, new WorldObject.PhysicThreits(), vulnarability);


        if (gameObject.GetComponent<HeroInformation>().Player == 1)
            ChoseCard.gmPlayer[gameObject.GetComponent<HeroInformation>().Index] = gameObject;
        else if (gameObject.GetComponent<HeroInformation>().Player == 2)
            ChoseCard.gmEnemy[gameObject.GetComponent<HeroInformation>().Index] = gameObject;

    }

}

