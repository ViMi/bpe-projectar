﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CardSpecialAtack : MonoBehaviour
{

    private struct SpecialCharacteristick
    {
        public Color C { set; get; }
        public Vector3 V { set; get; }
        public int S { set; get; }
        public float Lt { set; get; }

        public SpecialCharacteristick(Color c, Vector3 v,int s,float lt)
        {
            C = c;
            V = v;
            S = s;
            Lt = lt;

        }
    }

    private static SpecialCharacteristick SpecialChar(string name, GameObject gm)
    {
        switch (name)
        {
            case "Doctor": { return new SpecialCharacteristick(Color.yellow, gm.transform.position, 20,1.0f); }break;
            case "Priest": { return new SpecialCharacteristick(Color.red, gm.transform.position, 20,1.0f); } break;
            case "FaryMan": { return new SpecialCharacteristick(Color.blue, gm.transform.position, 20,1.0f); } break;
            case "Foreigner": { return new SpecialCharacteristick(Color.green, gm.transform.position, 20,1.0f); } break;
            case "RedDeath": { return new SpecialCharacteristick(Color.white, gm.transform.position, 20,1.0f); } break;
        }
        return new SpecialCharacteristick(Color.clear, gm.transform.position, 0, 0.0f);
    }

    public static void SpecialAtack()
    {
        

        SpecialCharacteristick sp;
        bool turn_0 = (ChoseCard.Turn == 1 && ChoseCard.g[0].GetComponent<HeroInformation>().GetPowerSlider().value >= ChoseCard.g[0].GetComponent<HeroInformation>().hero.HeroWeapon.Consuming.SpecialConsume
            && ChoseCard.g[0].GetComponent<HeroInformation>().GetHealthSlider().value >0);
        bool turn_1 = (ChoseCard.Turn == 2 && ChoseCard.g[1].GetComponent<HeroInformation>().GetPowerSlider().value >= ChoseCard.g[1].GetComponent<HeroInformation>().hero.HeroWeapon.Consuming.SpecialConsume
            && ChoseCard.g[1].GetComponent<HeroInformation>().GetHealthSlider().value > 0);

        if (turn_0 || turn_1)
        {

            if (turn_0)
            {

                ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index].gameObject.GetComponent<HeroInformation>().hero.TakeDamage
                    (ChoseCard.gmPlayer[ChoseCard.g[0].gameObject.GetComponent<HeroInformation>().Index].gameObject.GetComponent<HeroInformation>().hero.SpecialAtack());

                ChoseCard.g[0].GetComponent<HeroInformation>().GetPowerSlider().value -=
                    ChoseCard.gmPlayer[ChoseCard.g[0].gameObject.GetComponent<HeroInformation>().Index].gameObject.GetComponent<HeroInformation>().hero.SpecialAtackConsume();
                ChoseCard.g[1].GetComponent<HeroInformation>().GetHealthSlider().value = ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index]
                    .gameObject.GetComponent<HeroInformation>().hero.mainThreits.ObjectHealt;


                sp = SpecialChar(ChoseCard.gmPlayer[ChoseCard.g[0].gameObject.GetComponent<HeroInformation>().Index].gameObject.GetComponent<HeroInformation>().hero.mainThreits.ObjectName,
                    ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index]);

                Camera.main.GetComponent<AnimationSpecial>().Change(sp.C, sp.V, sp.S, sp.Lt);
            }
            else if (turn_1)
            {

                ChoseCard.gmPlayer[ChoseCard.g[0].gameObject.GetComponent<HeroInformation>().Index].gameObject.GetComponent<HeroInformation>().hero.TakeDamage
                    (ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index].gameObject.GetComponent<HeroInformation>().hero.SpecialAtack());


                ChoseCard.g[1].GetComponent<HeroInformation>().GetPowerSlider().value -= ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index].
                    gameObject.GetComponent<HeroInformation>().hero.SpecialAtackConsume();

                ChoseCard.g[0].GetComponent<HeroInformation>().GetHealthSlider().value = ChoseCard.gmPlayer[ChoseCard.g[0].gameObject.GetComponent<HeroInformation>().Index].
                    gameObject.GetComponent<HeroInformation>().hero.mainThreits.ObjectHealt;


                sp = SpecialChar(ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index].
                    gameObject.GetComponent<HeroInformation>().hero.mainThreits.ObjectName, ChoseCard.gmPlayer[ChoseCard.g[0].gameObject.GetComponent<HeroInformation>().Index]);
                Camera.main.GetComponent<AnimationSpecial>().Change(sp.C, sp.V, sp.S, sp.Lt);
            }

            if (ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index] != null) ChoseCard.Turn = ChoseCard.Turn == 1 ? 2 : 1;
            if (ChoseCard.Turn == 2 && ChoseCard.gmEnemy[ChoseCard.g[1].gameObject.GetComponent<HeroInformation>().Index] != null) Camera.main.GetComponent<EnemyTurn>().Enemy();

        }

    }

    private void OnMouseDown() {
        SpecialAtack();
        Camera.main.GetComponent<Death>().DeathCheck(); }

}
