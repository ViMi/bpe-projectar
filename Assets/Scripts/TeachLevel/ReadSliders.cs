﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using MMGWorld;
using UnityEngine.UI;

public class ReadSliders : MonoBehaviour
{

    public static Slider[] SlidersInit(GameObject gameObj)
    {

        Slider[] sliders = gameObj.GetComponentsInChildren<Slider>();

        bool IsHave = sliders[0] ?? false;
        bool IsHavePower = sliders[1] ?? false;

        sliders[0].maxValue =gameObj.GetComponent<HeroInformation>().hero.mainThreits.ObjectHealt;
        sliders[0].minValue = 0;


        sliders[1].maxValue = gameObj.GetComponent<HeroInformation>().hero.mainThreits.ObjectPower;
        sliders[1].minValue = 0;


        int max = (int)sliders[0].maxValue - 10;

        sliders[0].value = sliders[0].maxValue;
        sliders[1].value = sliders[1].maxValue;

        //Debug.Log("---__---MaxValueHealth--" + sliders[0].maxValue + "---__---MaxValuePower--" + sliders[1].maxValue + "---Max_" + max);
        //Debug.Log("Hero" + gameObj.GetComponent<HeroInformation>().hero.mainThreits.ObjectName);
        return sliders;
    }

}
