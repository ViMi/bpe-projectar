﻿using System.Collections;
using System.Collections.Generic;
using System;
using System.IO;
using UnityEngine;
using UnityEngine.Networking;
public class ReadCardSystem : MonoBehaviour
{
    
    static public string[] DeckOfCarts;
    static public string[] GlobalDeck;
    static public string[] DeckOfEnemy;


    [SerializeField]
    static public int CartPage;


    public string filePath = System.IO.Path.Combine(Application.streamingAssetsPath, "MyFile");
    public string result = "";


   

    IEnumerator GetRequest(string uri)
    {
        using (UnityWebRequest webRequest = UnityWebRequest.Get(uri))
        {
            yield return webRequest.SendWebRequest();

            if (!webRequest.isHttpError && !webRequest.isNetworkError)
            {
                DeckOfCarts = webRequest.downloadHandler.text.Split(':');
                Debug.Log("Error: " + webRequest.error);
            }
            else
                Debug.Log("Received: " + webRequest.downloadHandler.text);
        }
    }

    private void Awake()
    {
        if (PlayerPrefs.HasKey("DeckOfPlayer"))
        {
            DeckOfCarts = PlayerPrefs.GetString("DeckOfPlayer").Split(':');
        }
        else
        {
            PlayerPrefs.SetString("DeckOfPlayer", "Doctor:Foreigner:Priest:FaryMan");
            DeckOfCarts = PlayerPrefs.GetString("DeckOfPlayer").Split(':');
        }

        // DeckOfCarts = System.IO.Path.Combine(Application.streamingAssetsPath, "DeckOfCarts.txt").Split(':');
        //StartCoroutine(GetRequest(Application.persistentDataPath + @"\DeckOfCarts.txt"));
     //  DeckOfCarts = System.IO.File.ReadAllLines((Application.persistentDataPath + @"\Assets\DeckOfCarts.txt"));


        Debug.Log(Application.persistentDataPath+"-------");
        GlobalDeck = Resources.Load<TextAsset>("AllCart").text.Split(':');
        DeckOfEnemy = Resources.Load<TextAsset>("DeckOfCartsEnemy").text.Split(':');
       // for (int i = 0; i < DeckOfCarts.Length; i++)
         //   Debug.Log("-"+DeckOfCarts[i] + "--Player-");


        for (int i = 0; i < DeckOfEnemy.Length; i++)
            Debug.Log("-"+DeckOfEnemy[i] + "--Enemy-");


        for (int i = 0; i < GlobalDeck.Length; i++)
            Debug.Log("-"+GlobalDeck[i] + "--Global-");

    }
}
