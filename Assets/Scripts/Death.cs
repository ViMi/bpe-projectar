﻿using System.Collections;
using System.Collections.Generic;
using System.Threading;
using UnityEngine.UI;
using UnityEngine;

public class Death : MonoBehaviour
{
    [SerializeField]
    private Text TextPole = null;


    private int DeadHero = 0;
    private int DeadEnemy = 0;

    private void DisplayMenuExit()
    {

        if (DeadEnemy==1)
        {
            ActivateObject.Activate(Camera.main.GetComponent<ObjectsToActivate>().gm[1]);
            ActivateObject.Activate(Camera.main.GetComponent<ObjectsToActivate>().gm[0]);
            TextDisplay2.TextChange("Victory?", ref TextPole);
        }

        if (DeadHero == 4)
        {
            ActivateObject.Activate(Camera.main.GetComponent<ObjectsToActivate>().gm[1]);
            ActivateObject.Activate(Camera.main.GetComponent<ObjectsToActivate>().gm[0]);
            TextDisplay2.TextChange("Defeat!", ref TextPole);

        }
    }


    public void DeathCheck()
    {
        try
        {

            if (ChoseCard.g[1].GetComponent<HeroInformation>().GetHealthSlider().value <= 0)
            {
                ChoseCard.g[1].GetComponentInChildren<Renderer>().material = Resources.Load<Material>("Materials/Death");
                DeadEnemy += 1;
            }

            if (ChoseCard.g[0].GetComponent<HeroInformation>().GetHealthSlider().value <= 0)
            {
                ChoseCard.g[0].GetComponentInChildren<Renderer>().material = Resources.Load<Material>("Materials/Death");
                DeadHero += 1;
                Debug.Log("MagicMagicaaaaaa--" + DeadHero);
            }
        }

        catch
        {

        }
        DisplayMenuExit();
    }

}
