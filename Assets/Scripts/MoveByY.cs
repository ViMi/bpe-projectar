﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByY : MonoBehaviour
{
    [SerializeField]
    private float Y = 0.0f;

    private void Move(Vector3 vector) =>
        gameObject.transform.position += vector;

    private void OnMouseEnter() =>
        Move(new Vector3(0.0f, Y, 0.0f));


    private void OnMouseExit() =>
                Move(new Vector3(0.0f, -Y, 0.0f));
}
