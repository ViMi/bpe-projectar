﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrbBehavior : MonoBehaviour
{
    [SerializeField]
    private Animator anim;
    public GameObject Player;
    public GameObject Orb;

    private void Start()
    {
        anim = GetComponent<Animator>();
    }

    private void Update()
    {
      anim.SetBool("Take", true);
    }

    private void OnTriggerEnter(Collider other)
    {
       Destroy(Orb);
    }



}
