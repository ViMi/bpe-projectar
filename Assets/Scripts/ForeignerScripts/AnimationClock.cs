﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationClock : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    void Right() => transform.position += Vector3.right * 3;
    void Left() => transform.position += Vector3.left * 3;
    void Down() => transform.position += Vector3.down * 3;
    void Up() => transform.position += Vector3.up * 3;

    // Update is called once per frame
    void Update()
    {
        Invoke("Right", 1.5f); 
        Invoke("Left", 1.5f); 
        Invoke("Up", 1.5f); 
        Invoke("Down", 1.5f); 
    }
}
