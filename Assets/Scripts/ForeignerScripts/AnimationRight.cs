﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationRight : MonoBehaviour
{
    [SerializeField]
    private float Z = 0;
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        transform.rotation = Quaternion.Euler(0, 0, Z);
        Z += 1f;
        if (Z == float.MaxValue) Z = 0f;
    }
}
