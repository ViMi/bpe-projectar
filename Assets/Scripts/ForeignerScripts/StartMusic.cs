﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StartMusic : MonoBehaviour
{
    // Start is called before the first frame update
    public AudioClip impact;
    [SerializeField]
    private AudioSource audioSource;

  
    void Start()
    {
        audioSource = GetComponent<AudioSource>();

    }

    private void OnMouseEnter()
    {
        audioSource.PlayOneShot(impact, 0.8f);
    }

}
