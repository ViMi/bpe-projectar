﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainHeroFirstLevel : MonoBehaviour
{

    [SerializeField]
    private Vector3 forvard;
    [SerializeField]
    private Vector3 startPosition;
    [SerializeField]
    private float spead = 1.0f;

    [SerializeField]
    private bool isFacingRight = true;


    public static Animator anim;


    void Start()
    {
        startPosition = transform.position;
        anim = GetComponent<Animator>();

    }


    void Update()
    {
        Control();

    }

    public static int I = 1;


    public void Control()
    {
        if (Input.GetKey(KeyCode.RightArrow) || Input.GetKey(KeyCode.LeftArrow) || I ==1||I==-1)
        {

            if (Input.GetKey(KeyCode.RightArrow)||I==1)
            {

                if (!isFacingRight)
                {
                    isFacingRight = true;
                    Vector3 theScale = transform.localScale;
                    theScale.x *= -1;
                    transform.localScale = theScale;
                }

                anim.SetFloat("Spead", Mathf.Abs(spead));
                isFacingRight = true;
                forvard = Vector3.right;
                transform.position += forvard * spead*Time.deltaTime; 

            }
            if (Input.GetKey(KeyCode.LeftArrow)||I==-1)
            {
                if (isFacingRight)
                {
                    isFacingRight = !isFacingRight;
                    Vector3 theScale = transform.localScale;
                    theScale.x *= -1;
                    transform.localScale = theScale;
                }

                anim.SetFloat("Spead", Mathf.Abs(spead));
                forvard = Vector3.left;
                transform.position += forvard * spead*Time.deltaTime;
            }

         
          
        }
        else anim.SetFloat("Spead", Mathf.Abs(0));



        //if (Input.GetKey(KeyCode.A))
        //{
        //    RaycastHit rc;
        //    Vector3 fwd = transform.TransformDirection(Vector3.right);
        //    if(Physics.Raycast(transform.position, fwd, out rc, 100.0f))
        //    {
        //        if (rc.collider.GetComponent<Rigidbody>() != null)
        //        {
        //            if (rc.collider.gameObject.name == "Enemy")
        //            {
        //                Destroy(GameObject.Find("Enemy"));
        //            }
                    
        //        }
        //    }

        //}


    }

}
