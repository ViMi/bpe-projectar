﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Enemy : MonoBehaviour
{
    // Start is called before the first frame update

    public Transform player;
    public float move_speed=3f;
    public float rotation_speed=10f;
    public Transform enemy;
    public Animator anim;
    float spead = 1.2f;

    void Start()
    {
        anim = GetComponent<Animator>();
    }

    // Update is called once per frame
    void Update()
    {

            enemy.transform.position = Vector3.MoveTowards(enemy.transform.position, player.transform.position, 1);
            anim.SetFloat("Spead", spead);
            Quaternion target = Quaternion.LookRotation(player.transform.position - enemy.transform.position);
            enemy.rotation = Quaternion.Slerp(enemy.transform.rotation, target, 20 * Time.deltaTime);

    }

    private void OnTriggerEnter(Collider other)
    {
     //   if(other.gameObject.name=="MainHero")
    }

}
