﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationLeft : MonoBehaviour
{
    // Start is called before the first frame update
    [SerializeField]
    private float Z = 0;

    void Update()
    {
        transform.rotation = Quaternion.Euler(0,0,Z);
        Z -=1f;
        if (Z == float.MaxValue) Z = 0f;
    }
}
