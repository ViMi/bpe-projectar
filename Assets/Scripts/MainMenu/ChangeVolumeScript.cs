﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeVolumeScript : MonoBehaviour
{

    [SerializeField]
    private float MaxSliderX;
    [SerializeField]
    private float MinSliderX;

    private int IntValue;
    
    private void Start()
    {
        IntValue = (int)transform.position.z;
        AudioListener.volume = (Mathf.Abs(transform.position.z)-(Mathf.Abs(MaxSliderX)+Mathf.Abs(MinSliderX)))-IntValue;
    }
    




}
