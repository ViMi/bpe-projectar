﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCamera : MonoBehaviour
{

    [SerializeField]
    private Camera StartCamera;
    [SerializeField]
    private Camera SecondCAmera;

    private void OnMouseDown(){ Exchange(); }

    public void Exchange()
    {
        StartCamera.enabled = false;
        SecondCAmera.enabled = true;
    }
}
