﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ClossAppScript : MonoBehaviour
{
    private void OnMouseDown() => Application.Quit(0);

    public void CloseApp() => Application.Quit(0);
}
