﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeEat : MonoBehaviour
{

    private Animator Anim = new Animator();
    [SerializeField]
    private GameObject snake;

    private void OnMouseDown()
    {
        Anim = snake.GetComponent<Animator>();
        Anim.SetBool("SnakeEat", true);
        Anim.SetFloat("Spead", 1.0F);
    }
}
