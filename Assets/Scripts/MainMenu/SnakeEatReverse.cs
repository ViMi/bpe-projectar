﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SnakeEatReverse : MonoBehaviour
{
    private Animator Anim = new Animator();
    [SerializeField]
    private GameObject snake;

    private void OnMouseDown()
    {
        Anim = snake.GetComponent<Animator>();
        Anim.SetFloat("Spead", -1.0F);
    }
}
