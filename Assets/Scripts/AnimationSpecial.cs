﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationSpecial : MonoBehaviour
{
    [SerializeField]
    private ParticleSystem particleEffect;
    private float LiveTime;


    public  void Change(Color c, Vector3 v, int s,float time)
    {
        particleEffect.startColor = c;
        particleEffect.transform.position = v;
        particleEffect.startSpeed = s;
        particleEffect.Play();
        LiveTime = time;
    }

    private void Start() => particleEffect.Pause();

    private void Update() { if ((LiveTime = (LiveTime - Time.deltaTime)) <= 0) { particleEffect.Clear(); }

    }

}
