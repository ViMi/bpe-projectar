﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByZ : MonoBehaviour
{

    [SerializeField]
    private float Z = 0.0f;
    private void OnMouseEnter()=>gameObject.transform.position += new Vector3(0.0f, 0.0f, Z);
    private void OnMouseExit() => gameObject.transform.position -= new Vector3(0.0f, 0.0f, Z);
}
