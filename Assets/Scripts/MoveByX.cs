﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MoveByX : MonoBehaviour
{

    [SerializeField]
    private float X = 0.0f;
    private float Y = 0.0f;
    private float Z = 0.0f;
    private float Angele = 0.0f;

  public void MoveX() { gameObject.transform.position += new Vector3(X,0.0f,0.0f); }
  public void MoveY() { gameObject.transform.position += new Vector3(0.0f, Y, 0.0f); }
  public void MoveZ() { gameObject.transform.position += new Vector3(0.0f, 0.0f, Z); }

    public void MoveEulerX() { }
    public void MoveEulerY() { }
    public void MoveEulerZ() { }

}
