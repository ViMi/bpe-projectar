﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ActivateObject : MonoBehaviour
{

    [SerializeField]
    private GameObject[] Obj;
    public void ActivateDeactivate(int index) => Obj[index].SetActive(!Obj[index].activeSelf);



    public static void Activate(GameObject Obj) => Obj.SetActive(!Obj.activeSelf);

    private void OnMouseDown() => ActivateDeactivate(0);
}
