﻿using System.Collections;
using System.Collections.Generic;
using MMGWorld;
using UnityEngine.UI;
using UnityEngine;

public class HeroInformation : MonoBehaviour
{
    public int Player = 1;
    public int Index = 1;
    public Hero hero = null;
    private Slider[] sliders = new Slider[2]{null,null};


    public int Type = 1;


    private void Start()
    {

        if (Type == 1)
        {
            ReadCard.HeroInit(gameObject);
            sliders = ReadSliders.SlidersInit(gameObject);


            if (Player == 1)
                ChoseCard.gmPlayer[Index] = gameObject;
            else if (Player == 2)
                ChoseCard.gmEnemy[Index] = gameObject;
        }
    }




    public Slider GetHealthSlider() => sliders[0] ?? null;
    public Slider GetPowerSlider() => sliders[1] ?? null;



    public float GetHealthSlideValue() => sliders[0] == null?0.0f: sliders[0].value;
    public float GetPowerSliderValue() => sliders[1] == null ? 0.0f : sliders[1].value;
}
