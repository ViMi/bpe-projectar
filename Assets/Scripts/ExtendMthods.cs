﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.UI;
public static class ExtendMthods
{

    public static Transform[] ChildsWithTag(this Transform transform, string tag)=> transform.GetComponentsInChildren<Transform>().Where(t => t.tag == tag).ToArray();

    public static Transform ChildWithTag(this Transform transform, string tag)=> tag==transform.tag?transform.GetComponentInChildren<Transform>():null;

    public static Slider SliderWithTeg(this Transform slider, string tag)=> tag == slider.tag ? slider.GetComponentInChildren<Slider>() : null;


}
