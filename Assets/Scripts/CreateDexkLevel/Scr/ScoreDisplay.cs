﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreDisplay : MonoBehaviour
{
    private void OnGUI() => GUI.Label(Rect.zero, "Your Score --" + GlobalScore.Score.ToString());
}
