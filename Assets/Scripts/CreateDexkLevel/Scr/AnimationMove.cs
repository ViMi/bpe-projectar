﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationMove : MonoBehaviour
{

    [SerializeField]
    private GameObject gm;

    [SerializeField]
    private float MoveX = 0;
    private float MoveY = 0;
    private float MoveZ = 0;


    public void MoveObject() => gm.transform.Translate(new Vector3(MoveX, MoveY, MoveZ));

}
