﻿using System.Collections;
using System.Collections.Generic;
using MMGWorld;
using System.IO;
using UnityEngine;

public class CreateDeck : MonoBehaviour
{
    // Start is called before the first frame update
    protected int Index=0;

    private void Start()
    {
        string path = Application.dataPath + @"\StreamingAssets" + @"\Angelus\HeroThreats\" + ReadCardSystem.GlobalDeck[Index * CartDisplay.Page] + ".txt";
        Material mat = Resources.Load("Materials/" + "TutorialScene/" + "Card" + ReadCardSystem.GlobalDeck[Index * CartDisplay.Page]) as Material;

        Renderer rend = gameObject.GetComponentInChildren<Renderer>();
        rend.material = mat;
    }
}
