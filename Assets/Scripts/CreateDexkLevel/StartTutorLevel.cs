﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine;

public class StartTutorLevel : MonoBehaviour
{
    public void StartLevel(string nameScene)
    {
        SceneManager.LoadScene(nameScene);
    }


}
