﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class TextDisplay : MonoBehaviour
{
    public TextAsset file;
    public Text[] uiText=new Text[1];
    private Dictionary<string, string> WorkDictionary;

    private void Start()
    {

        string text = file.text;
        string[] inform = text.Split('\n');
        WorkDictionary = new Dictionary<string, string>();
        for(int i = 0; i < inform.Length; i++)
        {
            string[] KeyValue = inform[i].Split(':');
            WorkDictionary.Add(KeyValue[0], KeyValue[1]);
        }


        ShowTextCartByIndex(0);
        ShowTextCartKeyByIndex(1);

    }


    public void ShowTextCart() => ShowText(ReadCardSystem.GlobalDeck[CartDisplay.Page]);
    public void ShowTextCartKey() => DisplayKey(ReadCardSystem.GlobalDeck[CartDisplay.Page]);
    

    public void ShowTextCartByIndex(int index) => ShowText(ReadCardSystem.GlobalDeck[CartDisplay.Page>1?CartDisplay.Page-1:0],index);
    public void ShowTextCartKeyByIndex(int index) => DisplayKey(ReadCardSystem.GlobalDeck[CartDisplay.Page > 1 ? CartDisplay.Page-1 : 0],index);


    public void ShowText(string key) { for (int i = 0; i < uiText.Length; i++) uiText[i].text = WorkDictionary[key]; }
    public void DisplayKey(string key) { for (int i = 0; i < uiText.Length; i++) uiText[i].text = key; }

    public void ShowText(string key, int index) { uiText[index].text = WorkDictionary[key]; }
    public void DisplayKey(string key, int index) { uiText[index].text = key; }
}
