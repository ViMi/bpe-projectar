﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ChangeCart : MonoBehaviour
{

    public void Change()
    {

        if ((ChoseCard.g[1].GetComponent<HeroInformation>().Index * CartDisplay.Page) < ReadCardSystem.GlobalDeck.Length)
        {
            ReadCardSystem.DeckOfCarts[ChoseCard.g[0].GetComponent<HeroInformation>().Index] = ReadCardSystem.GlobalDeck[ChoseCard.g[1].GetComponent<HeroInformation>().Index * CartDisplay.Page];
            //  Debug.Log(ReadCardSystem.GlobalDeck[ChoseCard.g[1].GetComponent<HeroInformation>().Index * CartDisplay.Page] + "-----" + ChoseCard.g[1].GetComponent<HeroInformation>().Index * ReadCardSystem.CartPage);
            ChoseCard.g[0].GetComponentInChildren<Renderer>().material = ChoseCard.g[1].GetComponentInChildren<Renderer>().material;
        }
    }

}
