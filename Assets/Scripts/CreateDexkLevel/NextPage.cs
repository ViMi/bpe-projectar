﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NextPage : MonoBehaviour
{
    private int Pages;
    private void Start()
    {
        Pages = (ReadCardSystem.GlobalDeck.Length % CartDisplay.CountOfElements) == 0 ? ReadCardSystem.GlobalDeck.Length / CartDisplay.CountOfElements : (ReadCardSystem.GlobalDeck.Length / CartDisplay.CountOfElements) + 1;
        Debug.Log(Pages);
    }

    public void Next(int Change)=>
            CartDisplay.Page = ((CartDisplay.Page + Change) > Pages) || ((CartDisplay.Page + Change) <= 0) ? CartDisplay.Page : CartDisplay.Page + Change;
}
