﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadPlayerDeck : MonoBehaviour
{

    [SerializeField]
    private GameObject[] g = new GameObject[4];


    private void Start()
    {

        for (int i = 0; i < 4; i++)
        {  try
            {
                g[i].GetComponentInChildren<Renderer>().material = Resources.Load("Materials/Card"+ReadCardSystem.DeckOfCarts[i]) as Material;
            }
            catch
            {
                g[i].GetComponentInChildren<Renderer>().material = Resources.Load("Materials/EmptyCart") as Material;

            }
        }
    }
    
   
}
