﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
public class AudioPause : MonoBehaviour
{
    [SerializeField]
    private Toggle toggle;

    public void PauseAudioSource() => Camera.main.GetComponent<AudioSource>().volume = toggle.isOn == true ? 0 : 100;
}
