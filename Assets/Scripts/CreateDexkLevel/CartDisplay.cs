﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CartDisplay : MonoBehaviour
{


    public static int Page=1;
    public static int CountOfElements;
    [SerializeField]
    private GameObject[] g = new GameObject[4];

    private void Start() { CountOfElements = g.Length; ChangePage(); }

  
    public void ChangePage()
    {
        CountOfElements = g.Length;

        for (int i = 0; i < CountOfElements; i++)
        { 
            try
            {
               g[i].GetComponentInChildren<Renderer>().material =
                    Resources.Load("Materials/Card" + ReadCardSystem.GlobalDeck[(CountOfElements == 1) ? Page == 1 ? 0 : Page - 1 : i * Page]) as Material; 
         
            }
            catch
            {
                g[i].GetComponentInChildren<Renderer>().material =
                    Resources.Load("Materials/EmptyCart") as Material;


            }
        }
    }

}
