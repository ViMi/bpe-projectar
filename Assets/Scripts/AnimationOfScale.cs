﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationOfScale : MonoBehaviour
{


    [SerializeField]
    private Vector3 startScale;
    [SerializeField]
    private Vector3 endScale;
    
    private void Start() => gameObject.transform.localScale = startScale;
    private void Update() => gameObject.transform.localScale = Vector3.Lerp(gameObject.transform.localScale, endScale, Time.deltaTime);



}
