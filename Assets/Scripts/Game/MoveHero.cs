﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


[RequireComponent(typeof(Rigidbody))]
public class MoveHero : MonoBehaviour
{
 
    [SerializeField]
    private float Speed = 5f;

    [SerializeField]
    private float JumpForce = 300f;

    //что бы эта переменная работала добавьте тэг "Ground" на вашу поверхность земли
    private bool IsGrounded;
    private Rigidbody Rigid;


    private Animator anim;

    void Start()
    {
        Rigid = GetComponent<Rigidbody>();
        anim = GetComponent<Animator>();
    }



    void Update()
    {


         if (Input.GetKeyUp(KeyCode.C)) { anim.SetBool("Atack", false); }
        if (Input.GetKeyDown(KeyCode.C))anim.SetBool("Atack", true);


        if (Input.GetKeyUp(KeyCode.W)) anim.SetFloat("Walk", 0f);
        if (Input.GetKey(KeyCode.W))
       {
            // float moveHorizontal = Input.GetAxis("Horizontal");
            // float moveVertical = Input.GetAxis("Vertical");
            // Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);
            anim.SetFloat("Spead", 1.0f);
            anim.SetFloat("Walk", 3f);
            transform.Translate(new Vector3(0,0,-1) * Speed * Time.deltaTime);

        }

         if (Input.GetKeyDown(KeyCode.D))
        {
            transform.Rotate(0, 90, 0);
        }
         if (Input.GetKeyDown(KeyCode.A))
        {
            transform.Rotate(0, -90, 0);
        }

    } 





    private void MovementLogic()
    {

        if (Input.GetKeyDown(KeyCode.C))
        {
            anim.SetBool("Atack", true);

        }

        else if(Input.GetKeyDown(KeyCode.W))
        {
            // float moveHorizontal = Input.GetAxis("Horizontal");
            // float moveVertical = Input.GetAxis("Vertical");

            // Vector3 movement = new Vector3(moveHorizontal, 0.0f, moveVertical);

        
            
            transform.Translate(Vector3.back * Speed * Time.deltaTime);
            anim.SetBool("Walk", true);

        }


        else if (Input.GetKeyDown(KeyCode.D))
        {
            transform.Rotate(0, 90, 0);
        }

        else if (Input.GetKeyDown(KeyCode.A))
        {
            transform.Rotate(0, -90, 0);
        }
    }


    private void JumpLogic()
    {
        if (Input.GetAxis("Jump") > 0)
        {
            if (IsGrounded)
            {
                Rigid.AddForce(Vector3.up * JumpForce);
            }
        }
    }


    void OnCollisionEnter(Collision collision)
    {
        IsGroundedUpate(collision, true);
    }

    void OnCollisionExit(Collision collision)
    {
        IsGroundedUpate(collision, false);
    }

    private void IsGroundedUpate(Collision collision, bool value)
    {
        if (collision.gameObject.tag == ("Ground"))
        {
            IsGrounded = value;
        }
    }



}
