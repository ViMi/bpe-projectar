﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HouseFire : MonoBehaviour
{

    [SerializeField]
    private GameObject obj;

    [SerializeField]
    private AudioClip audioClip;

    [SerializeField]
    private ParticleSystem particles;

    private AudioSource src;
    private void Start()
    {
        src = GetComponent<AudioSource>();
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.tag == "Doctor")
        {
            obj.active = true;
            src.clip = audioClip;
            src.volume = 1f;
            
            src.Play();
            particles.Play();
        }
    }
}
