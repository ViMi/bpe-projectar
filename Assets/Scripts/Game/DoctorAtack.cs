﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DoctorAtack : MonoBehaviour
{
    // Start is called before the first frame update

    [SerializeField]
    private GameObject weapon;

    [SerializeField]
    private Animator anim;

    private MoveWeapon pm;
    void Start()
    {
        anim = GetComponent<Animator>();
        pm = GetComponent<MoveWeapon>();
    }


    void Update()
    {
        if (Input.GetKeyDown("A"))
        {
            anim.SetBool("Atack", true);
            weapon.active = true;
            pm.StartMove();
        }   
    }
}
