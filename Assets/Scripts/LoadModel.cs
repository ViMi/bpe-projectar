﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LoadModel : MonoBehaviour
{
    [SerializeField]
    private GameObject[] Obj;

    public float X=0.0f;
    public float Y=0.0f;
    public float Z=0.0f;
    public float XRotation=0.0f;
    public float YRotation=0.0f;
    public float ZRotation=0.0f;
    public int Index=0;


    private static GameObject gameObj=null;
    private void Start()
    {
        for(int i=0;i<Obj.Length;i++)
            Obj[i].tag = "ListObject";

        LocateObjectCart();
    }

    public void LocateObject(float x,float y, float z, float Xrotation,float Yrotation, float Zrotation,int index) { Instantiate(Obj[index], new Vector3(x, y, z), Quaternion.Euler(Xrotation, Yrotation, Zrotation)); }
    public void LocateObject() { Instantiate(Obj[Index], new Vector3(X, Y, Z), Quaternion.Euler(XRotation, YRotation, ZRotation)); }


    public void LocateObjectCart() { gameObj=Instantiate(Obj[CartDisplay.Page-1], new Vector3(X, Y, Z), Quaternion.Euler(XRotation, YRotation, ZRotation)); }
    public void LocateObjectCartByList()
    {
        Destroy(gameObj);
        gameObj=Instantiate(Obj[CartDisplay.Page - 1], new Vector3(X, Y, Z), Quaternion.Euler(XRotation, YRotation, ZRotation));
    }
}
